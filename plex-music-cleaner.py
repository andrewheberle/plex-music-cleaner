#!/usr/bin/env python
import sqlite3
import datetime
import os, sys, codecs
import string
from mutagen.easyid3 import EasyID3
from mutagen.easymp4 import EasyMP4

def format_filename(s):
	filename = ''.join(c for c in s if c not in r'\/:*?"<>|')
	return filename

sys.stdout = codecs.getwriter('utf8')(sys.stdout)

if sys.platform.startswith('win32'):
	plexhome=os.path.join(os.getenv('LOCALAPPDATA'), 'Plex Media Server')
elif sys.platform.startswith('linux'):
	plexhome=os.path.join(os.path.expanduser('~plex'), 'Library', 'Application Support', 'Plex Media Server')
elif sys.platform.startswith('darwin'):
	plexhome=os.path.join(os.path.expanduser('~'), 'Library', 'Application Support', 'Plex Media Server')
else:
	print(u'Unknown platform: {platform}'.format(platform=sys.platform))
	sys.exit()

dbpath = os.path.join(plexhome, 'Plug-in Support', 'Databases', 'com.plexapp.plugins.library.db')

print ('{plexhome} : {dbpath}'.format(plexhome=plexhome, dbpath=dbpath))
try:
	db = sqlite3.connect(dbpath)
	db.row_factory = sqlite3.Row
except:
	print(u'Error opening database: {db}'.format(db=dbpath))
	try:
		dbpath = os.path.join(os.getcwd(),'db', 'com.plexapp.plugins.library.db')
		db = sqlite3.connect(dbpath)
		db.row_factory = sqlite3.Row
	except:
		print(u'Error opening database: {db}'.format(db=dbpath))
		sys.exit()

metatype={"mtype": 10}

for row in db.execute('SELECT track.title AS trackname, track.[index] AS tracknum, album.title AS albumname, artist.title AS artistname, parts.id AS fileid, parts.file AS filename, locations.root_path AS basedir FROM metadata_items AS track, metadata_items AS album, metadata_items AS artist, media_parts AS parts, media_items AS items, section_locations AS locations WHERE track.parent_id = album.id AND artist.id = album.parent_id AND parts.media_item_id = items.id AND track.id = items.metadata_item_id AND locations.library_section_id = items.library_section_id AND track.metadata_type = :mtype', metatype):
	trackname = row['trackname']
	if row['tracknum'] >= 1:
		tracknum = int(row['tracknum'])
	else:
		tracknum = int(0)
	albumname = row['albumname']
	artistname = row['artistname']
	fileid = row['fileid']
	basedir = row['basedir']
	filename = os.path.relpath(row['filename'], basedir)
	fname, fext =  os.path.splitext(filename)
	ndir = format_filename(u'{artist} - {album}'.format(artist=artistname, album=albumname))
	nfile = format_filename(u'{track:02d} - {title}{ext}'.format(track=tracknum, title=trackname, ext=fext))
	newfilename = os.path.join(ndir, nfile)
	if artistname != u'[Unknown Artist]' and albumname != u'[Unknown Album]' and tracknum > 0:
		updatetags = False
		if fext == '.mp3':
			try:
				audio = EasyID3(os.path.join(basedir, filename))
				t = unicode(trackname)
				if 'title' in audio:
					if audio['title'][0] != t:
						print(u'Current Title: {c}; New Title: {n}'.format(c=audio['title'],n=t))
						audio['title'] = t
						updatetags = True
				else:
					audio['title'] = t
					updatetags = True
				n = unicode(tracknum)
				if 'tracknumber' in audio:
					if audio['tracknumber'][0] != n:
						print(u'Current Track #: {c}; New Track #: {n}'.format(c=audio['tracknumber'],n=n))
						audio['tracknumber'] = n
						updatetags = True
				else:
					audio['tracknumber'] = n
					updatetags = True
				a = unicode(artistname)
				if 'artist' in audio:
					if audio['artist'][0] != a:
						print(u'Current Artist: {c}; New Artist: {n}'.format(c=audio['artist'],n=a))
						audio['artist'] = a
						updatetags = True
				else:
					audio['artist'] = a
					updatetags = True
				a = unicode(albumname)
				if 'album' in audio:
					if audio['album'][0] != a:
						print(u'Current Album: {c}; New Album: {n}'.format(c=audio['album'],n=a))
						audio['album'] = a
						updatetags = True
				else:
					audio['album'] = a
					updatetags = True
				if updatetags == True:
					print(u'Updating tags for {filename}'.format(filename=filename))
					try:
						audio.save()
					except:
						print(u'Error saving updated tags for {filename}'.format(filename=filename))
				else:
					print(u'No tag update required for {filename}'.format(filename=filename))
			except:
				print(u'Error with tag update on {f}'.format(f=filename))
		elif fext == '.mp4' or fext == '.m4a':
			audio = EasyMP4(os.path.join(basedir, filename))
							if 'title' in audio:
					if audio['title'][0] != t:
						print(u'Current Title: {c}; New Title: {n}'.format(c=audio['title'],n=t))
						audio['title'] = t
						updatetags = True
				else:
					audio['title'] = t
					updatetags = True
				n = unicode(tracknum)
				if 'tracknumber' in audio:
					if audio['tracknumber'][0] != n:
						print(u'Current Track #: {c}; New Track #: {n}'.format(c=audio['tracknumber'],n=n))
						audio['tracknumber'] = n
						updatetags = True
				else:
					audio['tracknumber'] = n
					updatetags = True
				a = unicode(artistname)
				if 'artist' in audio:
					if audio['artist'][0] != a:
						print(u'Current Artist: {c}; New Artist: {n}'.format(c=audio['artist'],n=a))
						audio['artist'] = a
						updatetags = True
				else:
					audio['artist'] = a
					updatetags = True
				a = unicode(albumname)
				if 'album' in audio:
					if audio['album'][0] != a:
						print(u'Current Album: {c}; New Album: {n}'.format(c=audio['album'],n=a))
						audio['album'] = a
						updatetags = True
				else:
					audio['album'] = a
					updatetags = True
			if updatetags == True:
				print(u'Updating tags for {filename}'.format(filename=filename))
				try:
					audio.save()
				except:
					print(u'Error saving updated tags for {filename}'.format(filename=filename))
			else:
				print(u'No tag update required for{filename}'.format(filename=filename))
		try:
			srcfile = os.path.join(basedir, filename)
			tgtfile = os.path.join(basedir, newfilename)
			if srcfile != tgtfile:
				print(u'Renaming {srcfile} to {tgtfile}...'.format(srcfile=srcfile, tgtfile=tgtfile))
				os.renames(srcfile, tgtfile)
			#print u'Updating database...'
			#upd = db.cursor()
			#try:
			#	upd.execute('UPDATE media_parts SET file = :tgtfile WHERE id = :id', {"tgtfile": tgtfile, "id": fileid})
			#except:
			#	print u'Error with update query.'

		except OSError as e:
			print(u'Error with os.renames call. Error number: {num}; Error msg: {msg}'.format(num=e.errno, msg=e.strerror))	

db.close()